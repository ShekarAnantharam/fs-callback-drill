/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

function fsUsage(fs){
    fs.readFile('./data/lipsum.txt','utf-8',(err,data)=>{
        if(err){
             throw err;}
           
            fs.writeFile('./data/lipsumInUpperCase.txt',data.toUpperCase(),err=>{if (err) throw err});
            fs.writeFile('./data/filenames.txt','lipsumInUpperCase.txt,',err=>{if (err) throw err});
    
        fs.readFile('./data/lipsumInUpperCase.txt','utf-8',(err,data)=>{
            if(err){
                throw err;}
            let lipsumInLowerCase=data.toLowerCase();

            console.log()
            fs.writeFile('./data/lipsumInLowerCase.txt',lipsumInLowerCase.split('.').join('\n'),err=>{if (err) throw err});
            fs.appendFile('./data/filenames.txt','lipsumInLowerCase.txt,',err=>{if (err) throw err});
    
            fs.readFile('./data/lipsumInLowerCase.txt','utf-8',(err,data)=>{
                if(err){
                    throw err;}
                let lipsumInLowerCase=data.split('\n');
                lipsumInLowerCase=lipsumInLowerCase.map(ele=>ele.trim())
                console.log(lipsumInLowerCase.sort())
                
        
                fs.writeFile('./data/lipsumSortedLowercase.txt',lipsumInLowerCase,err=>{if (err) throw err});
                fs.appendFile('./data/filenames.txt','lipsumSortedLowercase.txt',err=>{if (err) throw err});
        
            })
        })
    })
    setTimeout(()=>{
        fs.readFile('./data/filenames.txt','utf8',(err,data)=>{
            if(err)throw err;
        let filenames=data.split(',');
        //filenames.push('filenames.txt');
        filenames.map(file=>{
            fs.unlink(`./data/${file}`,err=>{if (err) throw err})
        })
        })
    },2000)
    
    
    
    //console.log(data)
}
module.exports=fsUsage;