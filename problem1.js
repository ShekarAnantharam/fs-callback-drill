/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


function createAndRemoveFiles(fs){
    fs.mkdir('random',{recursive:true},(err)=>{
        if (err) throw err});
    fs.writeFile('random/random.json','\"this is a random file\"',err=>{
        if (err) throw err})
        fs.writeFile('random2.json','\"this is a random file\"',err=>{
            if (err) throw err})
    setTimeout(()=>{
        fs.rmdir('random',{recursive:true},err=>{if(err) throw err});
        fs.unlink('random2.json',err=>{if (err) throw err})
    },2000)
}


module.exports=createAndRemoveFiles;